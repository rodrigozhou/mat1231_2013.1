#ifndef LINALGBASE_H
#define LINALGBASE_H

#include <limits>

typedef double number_t;
typedef unsigned int idx_t;

static number_t EPSILON = 1e-9;
static number_t NUM_ERR = std::numeric_limits<number_t>::max();
static number_t IDX_ERR = std::numeric_limits<idx_t>::max();

static int cmp_number_t(number_t a, number_t b, number_t eps = EPSILON)
{
    return a + eps > b ? b + eps > a ? 0 : 1 : -1;
}

#endif // LINALGBASE_H
