#ifndef VECTOR_H
#define VECTOR_H

#include "linalgbase.h"

class Vector
{
    idx_t n;
    number_t *v;

  public:
    Vector ();
    Vector (idx_t n);
    Vector (const Vector &o);
    ~Vector ();

    const number_t& operator() (idx_t i) const;
    number_t& operator() (idx_t i);

    Vector& operator= (const Vector &o);
    Vector& operator+= (const Vector &o);
    Vector& operator-= (const Vector &o);
    Vector& operator*= (const number_t &o);
    Vector& operator/= (const number_t &o);

    Vector operator+ (const Vector &o) const;
    Vector operator- (const Vector &o) const;
    number_t operator* (const Vector &o) const;
    Vector operator* (const number_t &o) const;
    Vector operator/ (const number_t &o) const;

    bool operator== (const Vector &o) const;

    idx_t getSize() const;

    number_t norm() const;
    Vector& normalize();

    bool isNull(number_t eps = EPSILON) const;

    static Vector null(idx_t n);
    static Vector random(idx_t n, number_t lo = -1.0, number_t hi = 1.0);

    void print(bool sci = false, int width = 16) const;
};

Vector operator* (const number_t &k, const Vector &v);

#endif // VECTOR_H
