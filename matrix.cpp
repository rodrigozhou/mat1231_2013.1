#include "matrix.h"

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <utility>

using namespace std;

Matrix::Matrix () : m(0), n(0), mat(NULL) {}

Matrix::Matrix (idx_t m, idx_t n) : m(m), n(n), mat(NULL)
{
    if (m != 0 && n != 0) {
        mat = new number_t[m * n];
        if (mat == NULL)
            throw runtime_error("unable to allocate memory");
    }
    else {
        m = n = 0;
    }
}

Matrix::Matrix (const Matrix &o) : m(o.m), n(o.n), mat(NULL)
{
    if (m != 0 && n != 0) {
        mat = new number_t[m * n];
        if (mat == NULL)
            throw runtime_error("unable to allocate memory");
        for (idx_t i = 0, sz = m * n; i < sz; i++)
            mat[i] = o.mat[i];
    }
}

Matrix::~Matrix ()
{
    delete[] mat;
    mat = NULL;
}

const number_t& Matrix::operator() (idx_t i, idx_t j) const
{
    if (i >= m || j >= n)
        throw out_of_range("index out of range");
    return mat[i * n + j];
}

number_t& Matrix::operator() (idx_t i, idx_t j)
{
    if (i >= m || j >= n)
        throw out_of_range("index out of range");
    return mat[i * n + j];
}

number_t Matrix::get_elem(idx_t i, idx_t j) const
{
    return (*this)(i, j);
}

number_t Matrix::set_elem(idx_t i, idx_t j, number_t val)
{
    return (*this)(i, j) = val;
}

Matrix& Matrix::operator= (const Matrix &o)
{
    if (this == &o)
        return *this;
    if (m != o.m || n != o.n) {
        if (m * n != o.m * o.n) {
            this->~Matrix();
            mat = new number_t[o.m * o.n];
            if (mat == NULL)
                throw runtime_error("unable to allocate memory");
        }
        m = o.m;
        n = o.n;
    }
    for (idx_t i = 0, sz = m * n; i < sz; i++)
        mat[i] = o.mat[i];
    return *this;
}

Matrix& Matrix::operator+= (const Matrix &o)
{
    if (m != o.m || n != o.n)
        throw logic_error("matrices of different sizes");
    for (idx_t i = 0, sz = m * n; i < sz; i++)
        mat[i] += o.mat[i];
    return *this;
}

Matrix& Matrix::operator-= (const Matrix &o)
{
    if (m != o.m || n != o.n)
        throw logic_error("matrices of different sizes");
    for (idx_t i = 0, sz = m * n; i < sz; i++)
        mat[i] -= o.mat[i];
    return *this;
}

Matrix Matrix::operator+ (const Matrix &o) const
{
    if (m != o.m || n != o.n)
        throw logic_error("matrices of different sizes");
    Matrix ret(m, n);
    for (idx_t i = 0, sz = m * n; i < sz; i++)
        ret.mat[i] = mat[i] + o.mat[i];
    return ret;
}

Matrix Matrix::operator- (const Matrix &o) const
{
    if (m != o.m || n != o.n)
        throw logic_error("matrices of different sizes");
    Matrix ret(m, n);
    for (idx_t i = 0, sz = m * n; i < sz; i++)
        ret.mat[i] = mat[i] - o.mat[i];
    return ret;
}

Matrix Matrix::operator* (const Matrix &o) const
{
    if (n != o.m)
        throw logic_error("matrices with incompatible sizes");
    Matrix ret(m, o.n);
    for (idx_t i = 0, retij = 0, a = m; a--; i += n) {
        for (idx_t j = 0; j < o.n; j++, retij++) {
            number_t x = 0.0;
            for (idx_t ik = i, kj = j, b = n; b--; ik++, kj += o.n)
                x += mat[ik] * o.mat[kj];
            ret.mat[retij] = x;
        }
    }
    return ret;
}

Vector Matrix::operator* (const Vector &o) const
{
    if (n != o.getSize())
        throw logic_error("matrix/vector with incompatible sizes");
    Vector ret(m);
    for (idx_t ij = 0, idx = 0; idx < m; idx++) {
        number_t x = 0.0;
        for (idx_t j = 0; j < n; ij++, j++)
            x += mat[ij] * o(j);
        ret(idx) = x;
    }
    return ret;
}

bool Matrix::operator== (const Matrix &o) const
{
    if (m != o.m || n != o.n)
        return false;
    for (idx_t i = 0, sz = m * n; i < sz; i++)
        if (mat[i] != o.mat[i])
            return false;
    return true;
}

pair<idx_t, idx_t> Matrix::getSize() const
{
    return make_pair(m, n);
}

idx_t Matrix::getRows() const
{
    return m;
}

idx_t Matrix::getCols() const
{
    return n;
}

Vector Matrix::getRow(idx_t i) const
{
    Vector ret(n);
    for (idx_t j = i * n, idx = 0; idx < n; j++, idx++)
        ret(idx) = mat[j];
    return ret;
}

void Matrix::setRow(idx_t i, const Vector &v)
{
    idx_t v_sz = v.getSize();
    for (idx_t j = i * n, idx = 0; idx < n; j++, idx++)
        mat[j] = idx < v_sz ? v(idx) : 0.0;
}

Vector Matrix::getColumn(idx_t j) const
{
    Vector ret(m);
    for (idx_t i = j, idx = 0; idx < m; i += n, idx++)
        ret(idx) = mat[i];
    return ret;
}

void Matrix::setColumn(idx_t j, const Vector &v)
{
    idx_t v_sz = v.getSize();
    for (idx_t i = j, idx = 0; idx < m; i += n, idx++)
        mat[i] = idx < v_sz ? v(idx) : 0.0;
}

Matrix Matrix::getSubmatrix(idx_t row, idx_t nrows, idx_t col, idx_t ncols) const
{
    if (row + nrows > m || col + ncols > n)
        throw logic_error("invalid parameters");
    Matrix ret(nrows, ncols);
    for (idx_t ridx = 0, idx = row * n + col, i = 0; i < nrows; i++, idx += n - ncols)
        for (idx_t j = 0; j < ncols; j++, ridx++, idx++)
            ret.mat[ridx] = mat[idx];
    return ret;
}

void Matrix::setSubmatrix(idx_t row, idx_t col, const Matrix &o)
{
    if (row + o.m > m || col + o.n > n)
        throw logic_error("invalid parameters");
    for (idx_t idx = row * n + col, oidx = 0, i = 0; i < o.m; i++, idx += n - o.n)
        for (idx_t j = 0; j < o.n; j++, idx++, oidx++)
            mat[idx] = o.mat[oidx];
}

void Matrix::setSubmatrix(idx_t row, idx_t nrows, idx_t col, idx_t ncols, number_t val)
{
    if (row + nrows > m || col + ncols > n)
        throw logic_error("invalid parameters");
    for (idx_t idx = row * n + col, i = 0; i < nrows; i++, idx += n - ncols)
        for (idx_t j = 0; j < ncols; j++, idx++)
            mat[idx] = val;
}

number_t Matrix::norm() const
{
    number_t s = 0.0;
    for (idx_t i = 0, sz = m*n; i < sz; i++)
        s += mat[i] * mat[i];
    return s;
}

Matrix Matrix::transpose() const
{
    Matrix ret(n, m);
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = 0; j < n; j++)
            ret(j, i) = (*this)(i, j);
    return ret;
}

bool Matrix::isSquare() const
{
    return m == n;
}

bool Matrix::isNull(number_t eps) const
{
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = 0; j < n; j++)
            if (cmp_number_t((*this)(i, j), 0.0, eps) != 0)
                return false;
    return true;
}

bool Matrix::isIdentity(number_t eps) const
{
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = 0; j < n; j++)
            if (cmp_number_t((*this)(i, j), (number_t) (i == j), eps) != 0)
                return false;
    return true;
}

bool Matrix::isSymmetric(number_t eps) const
{
    if (m != n)
        return false;
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = j+1; j < n; j++)
            if (cmp_number_t((*this)(i, j), (*this)(j, i), eps) != 0)
                return false;
    return true;
}

bool Matrix::isD(number_t eps) const
{
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = 0; j < n; j++)
            if (i != j && cmp_number_t((*this)(i, j), 0.0, eps) != 0)
                return false;
    return true;
}

bool Matrix::isL(number_t eps) const
{
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = i+1; j < n; j++)
            if (cmp_number_t((*this)(i, j), 0.0, eps) != 0)
                return false;
    return true;
}

bool Matrix::isU(number_t eps) const
{
    for (idx_t j = 0; j < n; j++)
        for (idx_t i = j+1; i < m; i++)
            if (cmp_number_t((*this)(i, j), 0.0, eps) != 0)
                return false;
    return true;
}

bool Matrix::isQ(number_t eps) const
{
    return (*this * this->transpose()).isIdentity(eps);
}

bool Matrix::isHessenberg(number_t eps) const
{
    for (idx_t i = 2; i < m; i++)
        for (idx_t j = 0; j < i-1; j++)
            if (cmp_number_t((*this)(i, j), 0.0, eps) != 0)
                return false;
    return true;
}

Matrix Matrix::identity(idx_t n)
{
    Matrix ret(n, n);
    for (idx_t i = 0; i < n; i++) {
        ret(i, i) = 1.0;
        for (idx_t j = i+1; j < n; j++)
            ret(i, j) = ret(j, i) = 0.0;
    }
    return ret;
}

Matrix Matrix::null(idx_t m, idx_t n)
{
    Matrix ret(m, n);
    for (idx_t i = 0, sz = m*n; i < sz; i++)
        ret.mat[i] = 0.0;
    return ret;
}

Matrix Matrix::random(idx_t m, idx_t n, number_t lo, number_t hi)
{
    // srand(time(NULL));
    if (cmp_number_t(lo, hi) > 0)
        hi = lo;
    number_t interval = hi - lo;
    Matrix ret(m, n);
    for (idx_t i = 0, sz = m*n; i < sz; i++)
        ret.mat[i] = lo + interval * ((number_t) ::random() / RAND_MAX);
    return ret;
}

Matrix Matrix::randomSymmetric(idx_t n, number_t lo, number_t hi)
{
    // srand(time(NULL));
    if (cmp_number_t(lo, hi) > 0)
        hi = lo;
    number_t interval = hi - lo;
    Matrix A(n, n);
    for (idx_t i = 0; i < n; i++) {
        A(i, i) = lo + interval * ((number_t) ::random() / RAND_MAX);
        for (idx_t j = i+1; j < n; j++)
            A(i, j) = A(j, i) = lo + interval * ((number_t) ::random() / RAND_MAX);
    }
    return A;
}

Matrix Matrix::randomPositive(idx_t n)
{
    Matrix A = Matrix::random(n, n);
    return A * A.transpose();
}

void Matrix::print(bool sci, int width) const
{
    for (idx_t i = 0; i < m; i++) {
        for (idx_t j = 0; j < n; j++)
            if (sci)
                printf("%*le", width, (*this)(i, j));
            else
                printf("%*lf", width, (*this)(i, j));
        puts("");
    }
}
