#include "sparsematrix.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <stdexcept>
#include <utility>

using namespace std;

SparseMatrix::SparseMatrix () : m(0), n(0), rows(NULL) {}

SparseMatrix::SparseMatrix (idx_t m, idx_t n) : m(m), n(n), rows(NULL)
{
    if (m != 0 && n != 0) {
        rows = new list_t[m];
        if (rows == NULL)
            throw runtime_error("unable to allocate memory");
        memset(rows, NULL, m * sizeof(list_t));
    }
    else {
        m = n = 0;
    }
}

SparseMatrix::SparseMatrix (const SparseMatrix &o) : m(o.m), n(o.n), rows(NULL)
{
    if (m != 0 && n != 0) {
        rows = new list_t[m];
        if (rows == NULL)
            throw runtime_error("unable to allocate memory");
        for (idx_t i = 0; i < m; i++) {
            rows[i] = NULL;
            node_t *oaux = o.rows[i], *aux = NULL;
            while (oaux != NULL) {
                if (aux == NULL)
                    aux = rows[i] = new node_t;
                else
                    aux = aux->next = new node_t;
                if (aux == NULL)
                    throw runtime_error("unable to allocate memory");
                aux->j = oaux->j;
                aux->val = oaux->val;
                aux->next = NULL;
                oaux = oaux->next;
            }
        }
    }
}

SparseMatrix::~SparseMatrix ()
{
    for (idx_t i = 0; i < m; i++) {
        while (rows[i] != NULL) {
            node_t *aux = rows[i];
            rows[i] = rows[i]->next;
            delete aux;
        }
    }
    delete[] rows;
    rows = NULL;
}

number_t SparseMatrix::get_elem(idx_t i, idx_t j) const
{
    if (i >= m || j >= n)
        throw out_of_range("index out of range");
    node_t *aux = rows[i];
    while (aux != NULL) {
        if (aux->j > j)
            return 0.0;
        if (aux->j == j)
            return aux->val;
        aux = aux->next;
    }
    return 0.0;
}

number_t SparseMatrix::set_elem(idx_t i, idx_t j, number_t val)
{
    if (i >= m || j >= n)
        throw out_of_range("index out of range");
    node_t *prev = NULL, *aux = rows[i];
    while (aux != NULL) {
        if (aux->j > j) {
            if (cmp_number_t(val, 0.0) == 0)
                return 0.0;
            if (prev == NULL)
                prev = rows[i] = new node_t;
            else
                prev = prev->next = new node_t;
            if (prev == NULL)
                throw runtime_error("unable to allocate memory");
            prev->next = aux;
            prev->j = j;
            return prev->val = val;
        }
        if (aux->j == j) {
            if (cmp_number_t(val, 0.0) == 0) {
                if (prev == NULL)
                    rows[i] = aux->next;
                else
                    prev->next = aux->next;
                delete aux;
                return 0.0;
            }
            return aux->val = val;
        }
        prev = aux;
        aux = aux->next;
    }
    if (cmp_number_t(val, 0.0) == 0)
        return 0.0;
    if (prev == NULL)
        aux = rows[i] = new node_t;
    else
        aux = prev->next = new node_t;
    if (aux == NULL)
        throw runtime_error("unable to allocate memory");
    aux->next = NULL;
    aux->j = j;
    return aux->val = val;
}

SparseMatrix& SparseMatrix::operator= (const SparseMatrix &o)
{
    if (this == &o)
        return *this;
    if (m != o.m) {
        this->~SparseMatrix();
        rows = new list_t[o.m];
        if (rows == NULL)
            throw runtime_error("unable to allocate memory");
        memset(rows, NULL, o.m * sizeof(list_t));
        m = o.m;
    }
    n = o.n;
    for (idx_t i = 0; i < m; i++) {
        node_t *oaux = o.rows[i], *aux = rows[i], *prev = NULL;
        while (oaux != NULL && aux != NULL) {
            aux->j = oaux->j;
            aux->val = oaux->val;
            oaux = oaux->next;
            prev = aux;
            aux = aux->next;
        }
        while (oaux != NULL) {
            if (prev == NULL)
                aux = rows[i] = new node_t;
            else
                aux = prev->next = new node_t;
            if (aux == NULL)
                throw runtime_error("unable to allocate memory");
            aux->j = oaux->j;
            aux->val = oaux->val;
            aux->next = NULL;
            oaux = oaux->next;
            prev = aux;
            aux = NULL;
        }
        while (aux != NULL) {
            node_t *tmp = aux;
            aux = aux->next;
            delete tmp;
        }
    }
    return *this;
}

SparseMatrix SparseMatrix::operator* (const SparseMatrix &o) const
{
    if (n != o.m)
        throw logic_error("matrices with incompatible sizes");
    SparseMatrix ret(m, o.n);
    for (idx_t i = 0; i < m; i++) {
        for (idx_t j = 0; j < o.n; j++) {
            number_t x = 0.0;
            node_t *aux = rows[i];
            while (aux != NULL) {
                x += aux->val * o.get_elem(aux->j, j);
                aux = aux->next;
            }
            ret.set_elem(i, j, x);
        }
    }
    return ret;
}

Vector SparseMatrix::operator* (const Vector &o) const
{
    if (n != o.getSize())
        throw logic_error("matrix/vector with incompatible sizes");
    Vector ret(m);
    for (idx_t i = 0; i < m; i++) {
        number_t x = 0.0;
        node_t *aux = rows[i];
        while (aux != NULL) {
            x += aux->val * o(aux->j);
            aux = aux->next;
        }
        ret(i) = x;
    }
    return ret;
}

pair<idx_t, idx_t> SparseMatrix::getSize() const
{
    return make_pair(m, n);
}

idx_t SparseMatrix::getRows() const
{
    return m;
}

idx_t SparseMatrix::getCols() const
{
    return n;
}

Vector SparseMatrix::getRow(idx_t i) const
{
    Vector ret = Vector::null(n);
    node_t *aux = rows[i];
    while (aux != NULL) {
        ret(aux->j) = aux->val;
        aux = aux->next;
    }
    return ret;
}

Vector SparseMatrix::getColumn(idx_t j) const
{
    Vector ret(m);
    for (idx_t i = 0; i < m; i++)
        ret(i) = get_elem(i, j);
    return ret;
}

number_t SparseMatrix::norm() const
{
    number_t s = 0.0;
    for (idx_t i = 0; i < m; i++) {
        node_t *aux = rows[i];
        while (aux != NULL) {
            s += aux->val * aux->val;
            aux = aux->next;
        }
    }
    return s;
}

bool SparseMatrix::isSquare() const
{
    return m == n;
}

bool SparseMatrix::isSymmetric(number_t eps) const
{
    if (m != n)
        return false;
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = j+1; j < n; j++)
            if (cmp_number_t(get_elem(i, j), get_elem(j, i), eps) != 0)
                return false;
    return true;
}

SparseMatrix SparseMatrix::random(idx_t m, idx_t n, idx_t thres,
        number_t lo, number_t hi)
{
    // srand(time(NULL));
    if (cmp_number_t(lo, hi) > 0)
        hi = lo;
    number_t interval = hi - lo;
    number_t p = (number_t) thres / n;
    SparseMatrix ret(m, n);
    for (idx_t i = 0; i < m; i++)
        for (idx_t j = 0; j < n; j++)
            if (i == j || (number_t) ::random() / RAND_MAX < p)
                ret.set_elem(i, j, lo + interval * ((number_t) ::random() / RAND_MAX));
    return ret;
}

SparseMatrix SparseMatrix::randomSymmetric(idx_t n, idx_t thres,
        number_t lo, number_t hi)
{
    // srand(time(NULL));
    if (cmp_number_t(lo, hi) > 0)
        hi = lo;
    number_t interval = hi - lo;
    number_t p = (number_t) thres / n;
    SparseMatrix ret(n, n);
    for (idx_t i = 0; i < n; i++) {
        ret.set_elem(i, i, lo + interval * ((number_t) ::random() / RAND_MAX));
        for (idx_t j = i+1; j < n; j++) {
            if ((number_t) ::random() / RAND_MAX < p) {
                number_t x = lo + interval * ((number_t) ::random() / RAND_MAX);
                ret.set_elem(i, j, x);
                ret.set_elem(j, i, x);
            }
        }
    }
    return ret;
}

void SparseMatrix::print(bool sci, int width) const
{
    for (idx_t i = 0; i < m; i++) {
        for (idx_t j = 0; j < n; j++)
            if (sci)
                printf("%*le", width, this->get_elem(i, j));
            else
                printf("%*lf", width, this->get_elem(i, j));
        puts("");
    }
}
