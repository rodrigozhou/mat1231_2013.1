#include "vector.h"

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <stdexcept>

using namespace std;

Vector::Vector () : n(0), v(NULL) {}

Vector::Vector (idx_t n) : n(n), v(NULL)
{
    if (n != 0) {
        v = new number_t[n];
        if (v == NULL)
            throw runtime_error("unable to allocate memory");
    }
}

Vector::Vector (const Vector &o) : n(o.n), v(NULL)
{
    if (n != 0) {
        v = new number_t[n];
        if (v == NULL)
            throw runtime_error("unable to allocate memory");
        for (idx_t i = 0; i < n; i++)
            v[i] = o.v[i];
    }
}

Vector::~Vector ()
{
    delete[] v;
    v = NULL;
}

const number_t& Vector::operator() (idx_t i) const
{
    if (i >= n)
        throw out_of_range("index out of range");
    return v[i];
}

number_t& Vector::operator() (idx_t i)
{
    if (i >= n)
        throw out_of_range("index out of range");
    return v[i];
}

Vector& Vector::operator= (const Vector &o)
{
    if (this == &o)
        return *this;
    if (n != o.n) {
        this->~Vector();
        n = o.n;
        v = new number_t[n];
        if (v == NULL)
            throw runtime_error("unable to allocate memory");
    }
    for (idx_t i = 0; i < n; i++)
        v[i] = o.v[i];
    return *this;
}

Vector& Vector::operator+= (const Vector &o)
{
    if (n != o.n)
        throw logic_error("vectors of different sizes");
    for (idx_t i = 0; i < n; i++)
        v[i] += o.v[i];
    return *this;
}

Vector& Vector::operator-= (const Vector &o)
{
    if (n != o.n)
        throw logic_error("vectors of different sizes");
    for (idx_t i = 0; i < n; i++)
        v[i] -= o.v[i];
    return *this;
}

Vector& Vector::operator*= (const number_t &o)
{
    for (idx_t i = 0; i < n; i++)
        v[i] *= o;
    return *this;
}

Vector& Vector::operator/= (const number_t &o)
{
    for (idx_t i = 0; i < n; i++)
        v[i] /= o;
    return *this;
}

Vector Vector::operator+ (const Vector &o) const
{
    if (n != o.n)
        throw logic_error("vectors of different sizes");
    Vector ret(n);
    for (idx_t i = 0; i < n; i++)
        ret.v[i] = v[i] + o.v[i];
    return ret;
}

Vector Vector::operator- (const Vector &o) const
{
    if (n != o.n)
        throw logic_error("vectors of different sizes");
    Vector ret(n);
    for (idx_t i = 0; i < n; i++)
        ret.v[i] = v[i] - o.v[i];
    return ret;
}

number_t Vector::operator* (const Vector &o) const
{
    if (n != o.n)
        throw logic_error("vectors of different sizes");
    number_t ret = 0.0;
    for (idx_t i = 0; i < n; i++)
        ret += v[i] * o.v[i];
    return ret;
}

Vector Vector::operator* (const number_t &o) const
{
    Vector ret(n);
    for (idx_t i = 0; i < n; i++)
        ret.v[i] = v[i] * o;
    return ret;
}

Vector Vector::operator/ (const number_t &o) const
{
    Vector ret(n);
    for (idx_t i = 0; i < n; i++)
        ret.v[i] = v[i] / o;
    return ret;
}

Vector operator* (const number_t &k, const Vector &v)
{
    return v * k;
}

bool Vector::operator== (const Vector &o) const
{
    if (n != o.n)
        return false;
    for (idx_t i = 0; i < n; i++)
        if (v[i] != o.v[i])
            return false;
    return true;
}

idx_t Vector::getSize() const
{
    return n;
}

number_t Vector::norm() const
{
    number_t ret = 0.0;
    for (idx_t i = 0; i < n; i++)
        ret += v[i] * v[i];
    return sqrt(ret);
}

Vector& Vector::normalize()
{
    number_t x = norm();
    for (idx_t i = 0; i < n; i++)
        v[i] /= x;
    return *this;
}

bool Vector::isNull(number_t eps) const
{
    for (idx_t i = 0; i < n; i++)
        if (cmp_number_t((*this)(i), 0.0, eps) != 0)
            return false;
    return true;
}

Vector Vector::null(idx_t n)
{
    Vector ret(n);
    for (idx_t i = 0; i < n; i++)
        ret(i) = 0.0;
    return ret;
}

Vector Vector::random(idx_t n, number_t lo, number_t hi)
{
    // srand(time(NULL));
    if (cmp_number_t(lo, hi) > 0)
        hi = lo;
    number_t interval = hi - lo;
    Vector ret(n);
    for (idx_t i = 0; i < n; i++)
        ret(i) = lo + interval * ((number_t) ::random() / RAND_MAX);
    return ret;
}

void Vector::print(bool sci, int width) const
{
    for (idx_t i = 0; i < n; i++)
        if (sci)
            printf("%*le", width, (*this)(i));
        else
            printf("%*lf", width, (*this)(i));
    puts("");
}
