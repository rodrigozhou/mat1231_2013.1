#ifndef MATRIX_H
#define MATRIX_H

#include "linalgbase.h"
#include "vector.h"

#include <utility>

class Matrix
{
    idx_t m, n;
    number_t *mat;

  public:
    Matrix ();
    Matrix (idx_t m, idx_t n);
    Matrix (const Matrix &o);
    ~Matrix ();

    const number_t& operator() (idx_t i, idx_t j) const;
    number_t& operator() (idx_t i, idx_t j);

    number_t get_elem(idx_t i, idx_t j) const;
    number_t set_elem(idx_t i, idx_t j, number_t val);

    Matrix& operator= (const Matrix &o);
    Matrix& operator+= (const Matrix &o);
    Matrix& operator-= (const Matrix &o);

    Matrix operator+ (const Matrix &o) const;
    Matrix operator- (const Matrix &o) const;
    Matrix operator* (const Matrix &o) const;
    Vector operator* (const Vector &o) const;

    bool operator== (const Matrix &o) const;

    std::pair<idx_t, idx_t> getSize() const;
    idx_t getRows() const;
    idx_t getCols() const;

    Vector getRow(idx_t i) const;
    void   setRow(idx_t i, const Vector &v);
    Vector getColumn(idx_t j) const;
    void   setColumn(idx_t i, const Vector &v);

    Matrix getSubmatrix(idx_t row, idx_t nrows, idx_t col, idx_t ncols) const;
    void setSubmatrix(idx_t row, idx_t col, const Matrix &o);
    void setSubmatrix(idx_t row, idx_t nrows, idx_t col, idx_t ncols, number_t val);

    number_t norm() const;
    Matrix transpose() const;

    bool isSquare() const;
    bool isNull(number_t eps = EPSILON) const;
    bool isIdentity(number_t eps = EPSILON) const;
    bool isSymmetric(number_t eps = EPSILON) const;
    bool isD(number_t eps = EPSILON) const;
    bool isL(number_t eps = EPSILON) const;
    bool isU(number_t eps = EPSILON) const;
    bool isQ(number_t eps = EPSILON) const;
    bool isHessenberg(number_t eps = EPSILON) const;

    static Matrix identity(idx_t n);
    static Matrix null(idx_t m, idx_t n);
    static Matrix random(idx_t m, idx_t n, number_t lo = -1.0, number_t hi = 1.0);
    static Matrix randomSymmetric(idx_t n, number_t lo = -1.0, number_t hi = 1.0);
    static Matrix randomPositive(idx_t n);

    void print(bool sci = false, int width = 16) const;
};

#endif // MATRIX_H
