# Álgebra Linear Numérica (MAT1231) - DMAT - PUC-Rio

Códigos desenvolvidos durante o curso.

## Compilação

    g++ -O2 -o main main.cpp vector.cpp matrix.cpp sparsematrix.cpp

## Testes unitários

É utilizado o framework de testes unitários [CATCH](https://github.com/philsquared/Catch).

Para executar os testes, simplesmente compile o arquivo `test.cpp` e execute o arquivo compilado.

    g++ -Wall -O2 -o testtest.cpp vector.cpp matrix.cpp sparsematrix.cpp
    ./test
