/**
 * QZ algorithm
 * Compute the generalized eigenvalues
 * Reference:
 * C. B. Moler, G. W. Stewart, An Algorithm For Generalized Matrix Eigenvalue Problems
 */

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <cassert>
#include <functional>
#include <algorithm>

#include "vector.h"
#include "matrix.h"
#include "linalg.h"

using namespace std;

bool qz_step_hh_A(Matrix &A, Matrix &B, idx_t j, idx_t qnt, number_t eps = EPSILON);
bool qz_step_hh_B(Matrix &A, Matrix &B, idx_t j, idx_t qnt, number_t eps = EPSILON);
bool qz_algorithm(Matrix A, Matrix B, Vector &lambda, idx_t niter = 1000,
        number_t eps = EPSILON);

bool qz_step_hh_A(Matrix &A, Matrix &B, idx_t j, idx_t qnt, number_t eps)
{
    // if qnt == 1 then annihilate A[j+1][j-1]
    // if qnt == 2 then annihilate A[j+1][j-1], A[j+2][j-1]
    // if qnt == 3 then annihilate A[j+1][j-1], A[j+2][j-1], A[j+3][j-1]
    // and so on

    idx_t n = A.getRows();
    Vector v(n);
    number_t norm_aj = 0.0, norm_v = 0.0, x;

    for (idx_t i = 0; i < qnt; i++) {
        x = v(j+1+i) = A(j+1+i, j-1);
        norm_aj += x * x;
    }
    norm_v = norm_aj;

    x = A(j, j-1);
    norm_aj = sqrt(norm_aj + x * x);

    if (cmp_number_t(norm_aj, 0.0, eps) == 0)
        return false;

    if (x > 0)
        v(j) = x - norm_aj;
    else
        v(j) = x + norm_aj;

    norm_v = sqrt(norm_v + v(j) * v(j));
    if (cmp_number_t(norm_v, 0.0, eps) == 0)
        return false;
    v /= norm_v;

    for (idx_t k = 0; k < n; k++) {
        number_t v_ak = 0.0, v_bk = 0.0;
        for (idx_t i = 0; i <= qnt; i++) {
            v_ak += v(j+i) * A(j+i, k);
            v_bk += v(j+i) * B(j+i, k);
        }
        for (idx_t i = 0; i <= qnt; i++) {
            A(j+i, k) -= 2 * v_ak * v(j+i);
            B(j+i, k) -= 2 * v_bk * v(j+i);
        }
    }

    return true;
}

bool qz_step_hh_B(Matrix &A, Matrix &B, idx_t i, idx_t qnt, number_t eps)
{
    // if qnt == 1 then annihilate B[i][i-1]
    // if qnt == 2 then annihilate B[i][i-1], B[i][i-2]
    // if qnt == 3 then annihilate B[i][i-1], B[i][i-2], B[i][i-3]
    // and so on

    idx_t n = A.getRows();
    Vector v(n);
    number_t norm_bi = 0.0, norm_v = 0.0, x;

    for (idx_t j = 0; j < qnt; j++) {
        x = v(i-1-j) = B(i, i-1-j);
        norm_bi += x * x;
    }
    norm_v = norm_bi;

    x = B(i, i);
    norm_bi = sqrt(norm_bi + x * x);
    if (cmp_number_t(norm_bi, 0.0, eps) == 0) {
        printf("zero in the diagonal of B: %u", i);
        return false;
    }

    if (x > 0)
        v(i) = x - norm_bi;
    else
        v(i) = x + norm_bi;

    norm_v = sqrt(norm_v + v(i) * v(i));
    if (cmp_number_t(norm_v, 0.0, eps) == 0)
        return true;
    v /= norm_v;

    for (idx_t k = 0; k < n; k++) {
        number_t v_ak = 0.0, v_bk = 0.0;
        for (idx_t j = 0; j <= qnt; j++) {
            v_ak += v(i-j) * A(k, i-j);
            v_bk += v(i-j) * B(k, i-j);
        }
        for (idx_t j = 0; j <= qnt; j++) {
            A(k, i-j) -= 2 * v_ak * v(i-j);
            B(k, i-j) -= 2 * v_bk * v(i-j);
        }
    }

    return true;
}

bool qz_algorithm(Matrix A, Matrix B, Vector &lambda, idx_t niter, number_t eps)
{
    if (!A.isSquare() || !B.isSquare() || A.getSize() != B.getSize())
        return false;

    idx_t n = A.getRows();
    Vector v(n);
    number_t norm_aj, norm_bj, norm_v, x;

    // reduce B to upper tringular form by Householder reflections: QB = R
    // after this loop, B = R and A = QA
    for (idx_t j = 0; j < n; j++) {
        norm_bj = norm_v = 0.0;

        for (idx_t i = j+1; i < n; i++) {
            x = v(i) = B(i, j);
            norm_bj += x * x;
        }
        norm_v = norm_bj;

        x = B(j, j);
        norm_bj = sqrt(norm_bj + x * x);
        if (cmp_number_t(norm_bj, 0.0, eps) == 0)
            return false;

        if (x > 0)
            v(j) = x - norm_bj;
        else
            v(j) = x + norm_bj;

        norm_v = sqrt(norm_v + v(j) * v(j));
        if (cmp_number_t(norm_v, 0.0, eps) == 0)
            continue;
        v /= norm_v;

        for (idx_t k = 0; k < n; k++) {
            number_t v_ak = 0.0;
            number_t v_bk = 0.0;
            for (idx_t i = j; i < n; i++) {
                v_ak += v(i) * A(i, k);
                if (k >= j)
                    v_bk += v(i) * B(i, k);
            }
            for (idx_t i = j; i < n; i++) {
                A(i, k) -= 2 * v_ak * v(i);
                if (k >= j)
                    B(i, k) -= 2 * v_bk * v(i);
            }
        }
    }

    // TODO remove debug
    assert(B.isU(1e-6));

    // reduce A to upper Hessenberg form while preserving the triangularity of B
    for (idx_t j = 0; j < n-1; j++) {
        for (idx_t i = n-2; i > j; i--) {
            // annihilate A[i+1][j]
            norm_aj = norm_v = 0.0;

            x = v(i+1) = A(i+1, j);
            norm_v = norm_aj = x * x;
            x = A(i, j);
            norm_aj = sqrt(norm_aj + x * x);
            if (cmp_number_t(norm_aj, 0.0, eps) == 0)
                continue;

            if (x > 0)
                v(i) = x - norm_aj;
            else
                v(i) = x + norm_aj;

            norm_v = sqrt(norm_v + v(i) * v(i));
            if (cmp_number_t(norm_v, 0.0, eps) == 0)
                continue;
            v /= norm_v;

            for (idx_t k = j; k < n; k++) {
                number_t v_ak = v(i) * A(i, k) + v(i+1) * A(i+1, k);
                A(i  , k) -= 2 * v_ak * v(i  );
                A(i+1, k) -= 2 * v_ak * v(i+1);

                if (k >= i) {
                    number_t v_bk = v(i) * B(i, k) + v(i+1) * B(i+1, k);
                    B(i  , k) -= 2 * v_bk * v(i  );
                    B(i+1, k) -= 2 * v_bk * v(i+1);
                }
            }

            // annihilate B[i+1][i]
            if (!qz_step_hh_B(A, B, i+1, 1, eps))
                return false;
        }
    }

    // TODO remove debug
    assert(A.isHessenberg(1e-6));
    assert(B.isU(1e-6));

    // QZ step
    for (idx_t iter = 0; iter < niter; iter++) {
        // TODO remove debug
        assert(A.isHessenberg(1e-6));
        assert(B.isU(1e-6));

        for (idx_t j = 0; j < n-1; j++) {
            if (j == 0) {
                // first iteration of QZ step
                // compute fictitious column -1 of A
                number_t a_11 = A(0, 0),
                         a_12 = A(0, 1),
                         a_21 = A(1, 0),
                         a_22 = A(1, 1),
                         a_32 = A(2, 1),
                         b_11 = B(0, 0),
                         b_12 = B(0, 1),
                         b_22 = B(1, 1),
                         a_mm = A(n-2, n-2),
                         a_mn = A(n-2, n-1),
                         a_nm = A(n-1, n-2),
                         a_nn = A(n-1, n-1),
                         b_mm = B(n-2, n-2),
                         b_mn = B(n-2, n-1),
                         b_nn = B(n-1, n-1);

                number_t a_10 = ((a_mm/b_mm - a_11/b_11) * (a_nn/b_nn - a_11/b_11) - a_mn/b_nn * a_nm/b_mm + a_nm/b_mm * b_mn/b_nn * a_11/b_11) * b_11/a_21 + a_12/b_22 - a_11/b_11 * b_12/b_22,
                         a_20 = (a_22/b_22 - a_11/b_11) - a_21/b_11 * b_12/b_22 - (a_mm/b_mm - a_11/b_11) - (a_nn/b_nn - a_11/b_11) + a_nm/b_mm * b_mn/b_nn,
                         a_30 = a_32/b_22;

                // annihilate A[1][-1] and A[2][-1]
                norm_aj = norm_v = 0.0;

                v(1) = a_20;
                norm_aj += a_20 * a_20;
                v(2) = a_30;
                norm_aj += a_30 * a_30;
                norm_v = norm_aj;
                norm_aj = sqrt(norm_aj + a_10 * a_10);
                if (cmp_number_t(norm_aj, 0.0, eps) == 0)
                    continue;

                if (a_10 > 0)
                    v(0) = a_10 - norm_aj;
                else
                    v(0) = a_10 + norm_aj;

                norm_v = sqrt(norm_v + v(0) * v(0));
                if (cmp_number_t(norm_v, 0.0, eps) == 0)
                    continue;
                v /= norm_v;

                for (idx_t k = 0; k < n; k++) {
                    number_t v_ak = v(0) * A(0, k) + v(1) * A(1, k) + v(2) * A(2, k);
                    A(0, k) -= 2 * v_ak * v(0);
                    A(1, k) -= 2 * v_ak * v(1);
                    A(2, k) -= 2 * v_ak * v(2);

                    number_t v_bk = v(0) * B(0, k) + v(1) * B(1, k) + v(2) * B(2, k);
                    B(0, k) -= 2 * v_bk * v(0);
                    B(1, k) -= 2 * v_bk * v(1);
                    B(2, k) -= 2 * v_bk * v(2);
                }

                // annihilate B[2][0] and B[2][1]
                if (!qz_step_hh_B(A, B, 2, 2, eps))
                    return false;

                // annihilate B[1][0]
                if (!qz_step_hh_B(A, B, 1, 1, eps))
                    return false;
            }
            else if (1 <= j && j < n-2) {
                // annihilate A[j+1][j-1] and A[j+2][j-1]
                if (qz_step_hh_A(A, B, j, 2, eps)) {
                    // annihilate B[j+2][j] and B[j+2][j+1]
                    if (!qz_step_hh_B(A, B, j+2, 2, eps))
                        return false;

                    // annihilate B[j+1][j]
                    if (!qz_step_hh_B(A, B, j+1, 1, eps))
                        return false;
                }
            }
            else {
                // last iteration of QZ step
                // annihilate A[n-1][n-3] = A[j+1][j-1]
                if (qz_step_hh_A(A, B, j, 1, eps)) {
                    // annihilate B[n-1][n-2] = B[j+1][j]
                    if (!qz_step_hh_B(A, B, n-1, 1, eps))
                        return false;
                }
            }
        }
    }

    // TODO remove debug
    assert(A.isHessenberg(1e-6));
    assert(B.isU(1e-6));

    puts("final matrices");
    puts("A");
    A.print(false, 10);
    puts("");
    puts("B");
    B.print(false, 10);
    puts("");

    lambda = Vector(n);
    for (idx_t i = 0; i < n; i++)
        lambda(i) = A(i, i) / B(i, i);

    return true;
}

int main()
{
    { // test with positive definite matrix
        idx_t n = 50;
        srandom(327841);
        Matrix A = Matrix::randomPositive(n);
        Matrix B = Matrix::randomPositive(n);
        Vector lambda;

        if (!qz_algorithm(A, B, lambda, 100))
            puts("fail qz");

        // for (idx_t i = 0; i < n; i++)
        //     printf("%d %16.10lf\n", i, lambda(i));
        // puts("");

        Matrix L, Lh, L_1, L_h;
        if (!cholesky_decomposition_2(B, L, Lh)) {
            puts("fail cholesky");
            return 0;
        }
        if (!inverse_L(L, L_1)) {
            puts("fail L_1");
            return 0;
        }
        if (!inverse_U(Lh, L_h)) {
            puts("fail L_h");
            return 0;
        }

        Matrix C = L_1 * A * L_h;
        Matrix Q, H, D, P;
        hessenberg_decomposition(C, Q, H);
        if (!diag_QR(H, D, P, 10000, 1e-5))
            puts("fail diag");

        puts("D");
        D.print(false, 10);
        puts("");

        // for (idx_t i = 0; i < n; i++)
        //     printf("%d %16.10lf\n", i, D(i, i));
        // puts("");

        number_t a[n], norm_diff = 0.0;
        for (idx_t i = 0; i < n; i++)
            a[i] = lambda(i);
        sort(a, a+n, greater<number_t>());

        puts("generalized eigenvalues");
        for (idx_t i = 0; i < n; i++) {
            printf("%2d %16.8lf %16.8lf %16.8lf\n", i, a[i], D(i, i),
                    abs(a[i] - D(i, i)));
            norm_diff += (a[i] - D(i, i)) * (a[i] - D(i, i));
        }
        printf("norm diff: %16.8lf\n", norm_diff);
    }

    return 0;
}
