all: test

test: test.cpp vector.cpp matrix.cpp sparsematrix.cpp
	g++ -Wall -O2 -o test.out test.cpp vector.cpp matrix.cpp sparsematrix.cpp

main: main.cpp vector.cpp matrix.cpp sparsematrix.cpp
	g++ -Wall -O2 -o main.out main.cpp vector.cpp matrix.cpp sparsematrix.cpp

testdbg: test.cpp vector.cpp matrix.cpp sparsematrix.cpp
	g++ -Wall -g -o test.out test.cpp vector.cpp matrix.cpp sparsematrix.cpp

maindbg: main.cpp vector.cpp matrix.cpp sparsematrix.cpp
	g++ -Wall -g -o main.out main.cpp vector.cpp matrix.cpp sparsematrix.cpp

clean:
	rm -rf test.out* main.out*
