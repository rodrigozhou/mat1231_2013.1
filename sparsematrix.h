#ifndef SPARSEMATRIX_H
#define SPARSEMATRIX_H

#include "linalgbase.h"
#include "vector.h"

#include <utility>

struct node_t {
    idx_t j;
    number_t val;
    node_t *next;
};

typedef node_t* list_t;

class SparseMatrix
{
    idx_t m, n;
    list_t *rows;

  public:
    SparseMatrix ();
    SparseMatrix (idx_t m, idx_t n);
    SparseMatrix (const SparseMatrix &o);
    ~SparseMatrix ();

    number_t get_elem(idx_t i, idx_t j) const;
    number_t set_elem(idx_t i, idx_t j, number_t val);

    SparseMatrix& operator= (const SparseMatrix &o);
    SparseMatrix operator* (const SparseMatrix &o) const;
    Vector operator* (const Vector &o) const;

    std::pair<idx_t, idx_t> getSize() const;
    idx_t getRows() const;
    idx_t getCols() const;

    Vector getRow(idx_t i) const;
    Vector getColumn(idx_t j) const;

    number_t norm() const;

    bool isSquare() const;
    bool isSymmetric(number_t eps = EPSILON) const;

    static SparseMatrix random(idx_t m, idx_t n, idx_t thres = 3,
            number_t lo = -1.0, number_t hi = 1.0);
    static SparseMatrix randomSymmetric(idx_t n, idx_t thres = 3,
            number_t lo = -1.0, number_t hi = 1.0);

    void print(bool sci = false, int width = 16) const;
};

#endif // SPARSEMATRIX_H
