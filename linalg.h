#ifndef LINALG_H
#define LINALG_H

#include "linalgbase.h"
#include "vector.h"
#include "matrix.h"
#include "sparsematrix.h"

#include <cmath>
#include <algorithm>

using namespace std;

/**
 * @brief LU decomposition of a matrix A
 */
bool LU_decomposition(const Matrix &A, Matrix &L, Matrix &U)
{
    if (!A.isSquare())
        return false;

    idx_t n = A.getRows();

    U = A;
    L = Matrix::identity(n);

    for (idx_t i = 0; i < n; i++) {
        number_t p = U(i, i);

        if (cmp_number_t(p, 0.0) == 0)
            return false;

        for (idx_t j = i+1; j < n; j++) {
            number_t l = L(j, i) = U(j, i) / p;
            U(j, i) = 0.0;

            for (idx_t k = i+1; k < n; k++)
                U(j, k) -= l * U(i, k);
        }
    }

    return true;
}

/**
 * @brief determinant of LU
 */
number_t det_LU(const Matrix &L, const Matrix &U)
{
    if (!L.isSquare() || !U.isSquare() || L.getRows() != U.getRows())
        return false;
    if (!L.isL() || !U.isU())
        return false;

    idx_t n = L.getRows();
    number_t det = 1.0;

    for (idx_t i = 0; i < n; i++)
        det *= L(i, i) * U(i, i);

    return det;
}

/**
 * @brief compute the inverse of a matrix L
 */
bool inverse_L(const Matrix &L, Matrix &L_1)
{
    if (!L.isSquare() || !L.isL())
        return false;

    idx_t n = L.getRows();
    L_1 = Matrix::null(n, n);

    for (idx_t i = 0; i < n; i++) {
        for (idx_t j = i; j >= 0 && j != IDX_ERR; j--) {
            number_t s = (number_t) (i == j);
            for (idx_t k = j+1; k <= i; k++)
                s -= L_1(i, k) * L(k, j);
            if (cmp_number_t(L(j, j), 0.0) == 0)
                return false;
            L_1(i, j) = s / L(j, j);
        }
    }

    return true;
}

/**
 * @brief compute the inverse of a matrix U
 */
bool inverse_U(const Matrix &U, Matrix &U_1)
{
    if (!U.isSquare() || !U.isU())
        return false;

    idx_t n = U.getRows();
    U_1 = Matrix::null(n, n);

    for (idx_t i = 0; i < n; i++) {
        for (idx_t j = i; j < n; j++) {
            number_t s = (number_t) (i == j);
            for (idx_t k = 0; k < j; k++)
                s -= U_1(i, k) * U(k, j);
            if (cmp_number_t(U(j, j), 0.0) == 0)
                return false;
            U_1(i, j) = s / U(j, j);
        }
    }

    return true;
}

/**
 * @brief compute the inverse of a matrix A
 */
bool inverse(const Matrix &A, Matrix &A_1)
{
    Matrix L, U, L_1, U_1;

    if (!LU_decomposition(A, L, U))
        return false;

    if (!inverse_L(L, L_1))
        return false;

    if (!inverse_U(U, U_1))
        return false;

    A_1 = U_1 * L_1;

    return true;
}

/**
 * @brief solve Lx = b
 */
bool solve_L(const Matrix &L, const Vector &b, Vector &x)
{
    if (!L.isSquare() || !L.isL())
        return false;

    idx_t n = L.getRows();
    x = Vector(n);

    for (idx_t i = 0; i < n; i++) {
        number_t s = 0.0;
        for (idx_t j = 0; j < i; j++)
            s += L(i, j) * x(j);
        if (cmp_number_t(L(i, i), 0.0) == 0)
            return false;
        x(i) = (b(i) - s) / L(i, i);
    }

    return true;
}

/**
 * @brief solve Ux = b
 */
bool solve_U(const Matrix &U, const Vector &b, Vector &x)
{
    if (!U.isSquare() || !U.isU())
        return false;

    idx_t n = U.getRows();
    x = Vector(n);

    for (idx_t i = n-1; i >= 0 && i != IDX_ERR; i--) {
        number_t s = 0.0;
        for (idx_t j = i+1; j < n; j++)
            s += U(i, j) * x(j);
        if (cmp_number_t(U(i, i), 0.0) == 0)
            return false;
        x(i) = (b(i) - s) / U(i, i);
    }

    return true;
}

/**
 * @brief solve LUx = b
 */
bool solve_LU(const Matrix &L, const Matrix &U, const Vector &b, Vector &x)
{
    if (!L.isSquare() || !U.isSquare() || L.getRows() != U.getRows())
        return false;
    if (!L.isL() || !U.isU())
        return false;

    Vector Ux;

    if (!solve_L(L, b, Ux))
        return false;

    if (!solve_U(U, Ux, x))
        return false;

    return true;
}

/**
 * @brief Cholesky decomposition of a matrix A
 */
bool cholesky_decomposition(const Matrix &A, Matrix &L, Matrix &Lt)
{
    if (!A.isSquare() || !A.isSymmetric())
        return false;

    idx_t n = A.getRows();

    L = A;
    Lt = Matrix(n, n);

    for (idx_t k = 0; k < n; k++) {
        number_t p = L(k, k);
        if (cmp_number_t(p, 0.0) < 0)
            return false;

        number_t q = sqrt(p);

        for (idx_t i = k+1; i < n; i++) {
            for (idx_t j = i; j < n; j++)
                L(j, i) -= L(i, k) * L(j, k) / p;

            Lt(k, i) = (L(i, k) /= q);
            Lt(i, k) = L(k, i) = 0.0;
        }

        Lt(k, k) = L(k, k) = q;
    }

    return true;
}

/**
 * @brief Cholesky decomposition of a matrix A (faster version)
 */
bool cholesky_decomposition_2(const Matrix &A, Matrix &L, Matrix &Lt)
{
    if (!A.isSquare() || !A.isSymmetric())
        return false;

    idx_t n = A.getRows();

    L = Lt = Matrix(n, n);

    for (idx_t j = 0; j < n; j++) {
        number_t s = A(j, j);
        for (idx_t k = 0; k < j; k++)
            s -= L(j, k) * L(j, k);

        if (cmp_number_t(s, 0.0) < 0)
            return false;

        number_t p = Lt(j, j) = L(j, j) = sqrt(s);

        for (idx_t i = j+1; i < n; i++) {
            s = A(i, j);
            for (idx_t k = 0; k < j; k++)
                s -= L(i, k) * L(j, k);
            Lt(j, i) = L(i, j) = s / p;
            Lt(i, j) = L(j, i) = 0.0;
        }
    }

    return true;
}

/**
 * @brief Cholesky block decomposition of a matrix A
 */
bool cholesky_block_decomposition(const Matrix &A, Matrix &L, idx_t block_sz)
{
    if (!A.isSquare() || !A.isSymmetric())
        return false;

    idx_t n = A.getRows();

    L = A;

    Matrix L00, L00L, L00Lt, L00Lt_1;
    for (idx_t k = 0; k+block_sz <= n; k += block_sz) {
        L00 = L.getSubmatrix(k, block_sz, k, block_sz);
        if (!cholesky_decomposition_2(L00, L00L, L00Lt))
            return false;
        L.setSubmatrix(k, k, L00L);

        idx_t sz = n - (k + block_sz);
        if (sz > 0) {
            if (!inverse_U(L00Lt, L00Lt_1))
                return false;

            Matrix L10 = L.getSubmatrix(k+block_sz, sz, k, block_sz) * L00Lt_1;
            L.setSubmatrix(k+block_sz, k, L10);
            L.setSubmatrix(k, block_sz, k+block_sz, sz, 0.0);

            Matrix L11 = L.getSubmatrix(k+block_sz, sz, k+block_sz, sz);
            L11 -= L10 * L10.transpose();
            L.setSubmatrix(k+block_sz, k+block_sz, L11);
        }
    }

    if (n % block_sz) {
        idx_t idx = n / block_sz * block_sz;
        L00 = L.getSubmatrix(idx, n % block_sz, idx, n % block_sz);
        if (!cholesky_decomposition_2(L00, L00L, L00Lt))
            return false;
        L.setSubmatrix(idx, idx, L00L);
    }

    return true;
}

/**
 * @brief Cholesky block decomposition of a matrix A (faster)
 */
bool cholesky_block_decomposition_2(const Matrix &A, Matrix &L, idx_t block_sz)
{
    if (!A.isSquare() || !A.isSymmetric())
        return false;

    idx_t n = A.getRows();
    idx_t sz = (n + block_sz - 1) / block_sz * block_sz;

    L = Matrix::identity(sz);
    L.setSubmatrix(0, 0, A);

    Matrix S, Ljj, Ljjt, Ljjt_1;
    Matrix Ljk;
    for (idx_t j = 0; j < sz; j += block_sz) {
        S = L.getSubmatrix(j, block_sz, j, block_sz);
        for (idx_t k = 0; k < j; k += block_sz) {
            Ljk = L.getSubmatrix(j, block_sz, k, block_sz);
            S -= Ljk * Ljk.transpose();
        }

        if (!cholesky_decomposition_2(S, Ljj, Ljjt))
            return false;

        L.setSubmatrix(j, j, Ljj);

        if (j + block_sz < sz) {
            if (!inverse_U(Ljjt, Ljjt_1))
                return false;

            for (idx_t i = j+block_sz; i < sz; i += block_sz) {
                S = L.getSubmatrix(i, block_sz, j, block_sz);
                for (idx_t k = 0; k < j; k += block_sz)
                    S -= L.getSubmatrix(i, block_sz, k, block_sz) *
                        L.getSubmatrix(j, block_sz, k, block_sz).transpose();
                L.setSubmatrix(i, j, S * Ljjt_1);
                L.setSubmatrix(j, block_sz, i, block_sz, 0.0);
            }
        }
    }

    if (sz != n)
        L = L.getSubmatrix(0, n, 0, n);

    return true;
}

/**
 * @brief QR decomposition using Gram-Schmidt orthonormalization
 */
bool QR_GS_decomposition(const Matrix &A, Matrix &Q, Matrix &R)
{
    if (!A.isSquare())
        return false;

    idx_t n = A.getRows();

    Q = Matrix(n, n);
    R = Matrix::null(n, n);

    Vector q_j, q_k, a_j;
    for (idx_t j = 0; j < n; j++) {
        q_j = a_j = A.getColumn(j);

        for (idx_t k = 0; k < j; k++) {
            q_k = Q.getColumn(k);

            number_t rkj = a_j * q_k;
            q_j -= rkj * q_k;

            R(k, j) = rkj;
        }

        number_t rjj = q_j.norm();
        if (cmp_number_t(rjj, 0.0) == 0)
            return false;

        R(j, j) = rjj;

        for (idx_t i = 0; i < n; i++)
            Q(i, j) = q_j(i) / rjj;
    }

    return true;
}

/**
 * @brief QR decomposition using Householder reflections
 * TODO fix the algorithm for the case when M > N
 */
bool QR_HH_decomposition(const Matrix &A, Matrix &Q, Matrix &R)
{
    idx_t m = A.getRows(), n = A.getCols();

    Q = Matrix::identity(m);
    R = A;

    Vector v(m);
    for (idx_t j = 0; j < min(m, n); j++) {
        number_t norm_v  = 0.0;
        number_t norm_rj = 0.0;
        for (idx_t i = j+1; i < m; i++) {
            number_t vij = v(i) = R(i, j);
            norm_rj += vij * vij;
        }
        norm_v = norm_rj;

        number_t rjj = R(j, j);
        norm_rj = sqrt(norm_rj + rjj * rjj);
        if (cmp_number_t(norm_rj, 0.0) == 0)
            return false;

        if (rjj >= 0.0)
            v(j) = rjj - norm_rj;
        else
            v(j) = rjj + norm_rj;

        norm_v = sqrt(norm_v + v(j) * v(j));
        if (cmp_number_t(norm_v, 0.0) == 0)
            continue;

        v /= norm_v;

        for (idx_t k = 0; k < max(m, n); k++) {
            number_t vrj = 0.0;
            number_t vqj = 0.0;
            for (idx_t i = j; i < m; i++) {
                if (k < n)
                    vrj += R(i, k) * v(i);
                if (k < m)
                    vqj += Q(k, i) * v(i);
            }
            for (idx_t i = j; i < m; i++) {
                if (k < n)
                    R(i, k) -= 2.0 * vrj * v(i);
                if (k < m)
                    Q(k, i) -= 2.0 * vqj * v(i);
            }
        }
    }

    return true;
}

/**
 * @brief solve QRx = b
 */
bool solve_QR(const Matrix &Q, const Matrix &R, const Vector &b, Vector &x)
{
    if (!Q.isSquare() || Q.getRows() != R.getRows())
        return false;
    if (!Q.isQ())
        return false;

    Matrix Qt = Q.transpose();
    Vector Qtb = Qt * b;

    if (!solve_U(R, Qtb, x))
        return false;

    return true;
}

/**
 * @brief diagonalization of a matrix using QR method
 */
bool diag_QR(const Matrix &A, Matrix &D, Matrix &P,
        idx_t niter = 1000, number_t eps = EPSILON)
{
    if (!A.isSquare())
        return false;

    Matrix Q, R;

    D = A;
    P = Matrix::identity(A.getRows());
    while (niter--) {
        if (!QR_HH_decomposition(D, Q, R))
            return false;

        D = R * Q;
        P = P * Q;
        if (D.isD(eps))
            return true;
    }

    return false;
}

/**
 * @brief Hessenberg decomposition using Householder reflections
 */
bool hessenberg_decomposition(const Matrix &A, Matrix &Q, Matrix &H)
{
    if (!A.isSquare())
        return false;

    idx_t n = A.getRows();

    Q = Matrix::identity(n);
    H = A;

    Vector v(n);
    for (idx_t j = 0; j < n-1; j++) {
        number_t rjj = 0.0;
        number_t norm = 0.0;
        for (idx_t i = j+2; i < n; i++) {
            number_t vij = v(i) = H(i, j);
            rjj += vij * vij;
            norm += vij * vij;
        }

        number_t vjj = H(j+1, j);
        rjj = sqrt(rjj + vjj * vjj);
        if (cmp_number_t(rjj, 0.0) == 0)
            return false;

        if (vjj >= 0.0)
            v(j+1) = vjj - rjj;
        else
            v(j+1) = vjj + rjj;

        norm = sqrt(norm + v(j+1) * v(j+1));
        if (cmp_number_t(norm, 0.0) == 0)
            continue;

        v /= norm;

        // left multiplication by householder vector v
        for (idx_t k = 0; k < n; k++) {
            number_t vhj = 0.0;
            for (idx_t i = j+1; i < n; i++)
                vhj += H(i, k) * v(i);
            for (idx_t i = j+1; i < n; i++)
                H(i, k) -= 2.0 * vhj * v(i);
        }

        // right multiplication by householder vector v
        for (idx_t k = 0; k < n; k++) {
            number_t vhj = 0.0;
            number_t vqj = 0.0;
            for (idx_t i = j+1; i < n; i++) {
                vhj += H(k, i) * v(i);
                vqj += Q(k, i) * v(i);
            }
            for (idx_t i = j+1; i < n; i++) {
                H(k, i) -= 2.0 * vhj * v(i);
                Q(k, i) -= 2.0 * vqj * v(i);
            }
        }
    }

    return true;
}

/**
 * @brief Lanczos method for eigenvalues and eigenvectors
 */
bool lanczos_eigenpairs(const SparseMatrix &A, Vector &lambda, Matrix &eigv, idx_t n)
{
    if (!A.isSymmetric())
        return false;

    idx_t sz = A.getCols();

    Vector alpha(n+1), beta(n+1);
    Vector v[n], vp, vj, vn, w;

    vj = Vector::null(sz);
    vn = Vector::random(sz);
    vn.normalize();

    beta(0) = 0.0;

    for (idx_t j = 0; j < n; j++) {
        vp = vj;
        vj = vn;
        v[j] = vj;

        w = A * vj;
        alpha(j) = w * vj;
        w = w - alpha(j) * vj - beta(j) * vp;
        beta(j+1) = w.norm();
        vn = w / beta(j+1);
    }

    Matrix AA = Matrix::null(n, n), D, P;
    AA(0, 0) = alpha(0);
    for (idx_t i = 1; i < n; i++) {
        AA(i, i) = alpha(i);
        AA(i, i-1) = AA(i-1, i) = beta(i);
    }

    if (!diag_QR(AA, D, P, 1000, 1e-4))
        return false;

    lambda = Vector(n);
    for (idx_t i = 0; i < n; i++)
        lambda(i) = D(i, i);

    eigv = Matrix(sz, n);
    for (idx_t k = 0; k < n; k++) {
        for (idx_t i = 0; i < sz; i++) {
            number_t x = 0.0;
            for (idx_t j = 0; j < n; j++)
                x += P(j, k) * v[j](i);
            eigv(i, k) = x;
        }
    }

    return true;
}

/**
 * @brief Arnoldi iteration for eigenvalues and eigenvectors
 */
bool arnoldi_eigenpairs(const SparseMatrix &A, Vector &lambda, Matrix &eigv, idx_t n)
{
    if (!A.isSquare())
        return false;

    idx_t sz = A.getCols();

    Matrix H = Matrix::null(n, n), D, P;
    Vector v[n+1];

    v[0] = Vector::random(sz);
    v[0].normalize();

    for (idx_t j = 1; j <= n; j++) {
        v[j] = A * v[j-1];
        for (idx_t i = 0; i < j; i++) {
            number_t x = v[i] * v[j];
            H(i, j-1) = x;
            v[j] -= x * v[i];
        }
        if (j < n) {
            number_t x = v[j].norm();
            if (cmp_number_t(x, 0.0) == 0)
                return false;
            H(j, j-1) = x;
            v[j] /= x;
        }
    }

    if (!diag_QR(H, D, P, 1000, 1e-4))
        return false;

    lambda = Vector(n);
    for (idx_t i = 0; i < n; i++)
        lambda(i) = D(i, i);

    eigv = Matrix(sz, n);
    for (idx_t k = 0; k < n; k++) {
        for (idx_t i = 0; i < sz; i++) {
            number_t x = 0.0;
            for (idx_t j = 0; j < n; j++)
                x += P(j, k) * v[j](i);
            eigv(i, k) = x;
        }
    }

    return true;
}

#endif // LINALG_H
