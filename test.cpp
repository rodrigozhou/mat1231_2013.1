#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "vector.h"
#include "matrix.h"
#include "sparsematrix.h"
#include "linalg.h"

#include <cstdio>
#include <utility>

using namespace std;

/* SECTION MATRIX */

TEST_CASE("matrix/accessors", "test getter and setter") {
    Matrix A(10, 10);

    A(1, 1) = 1;
    A(3, 2) = 10;

    REQUIRE(A(1, 1) == 1);
    REQUIRE(A(3, 2) == 10);
    REQUIRE_THROWS(A(11, 10) == 1);
}

TEST_CASE("matrix/sum_operator", "test sum of matrices") {
    Matrix A(2, 3), B(2, 3), C;

    A(0, 0) = 1, A(0, 1) = 4, A(0, 2) = 1;
    A(1, 0) = 9, A(1, 1) = 8, A(1, 2) = 0;

    B(0, 0) = 8, B(0, 1) = 6, B(0, 2) = 1;
    B(1, 0) = 9, B(1, 1) = 2, B(1, 2) = 7;

    C = A + B;
    REQUIRE(C(0, 0) == 9);
    REQUIRE(C(0, 1) == 10);
    REQUIRE(C(0, 2) == 2);
    REQUIRE(C(1, 0) == 18);
    REQUIRE(C(1, 1) == 10);
    REQUIRE(C(1, 2) == 7);
}

TEST_CASE("matrix/subtraction_operator", "test subtraction of matrices") {
    Matrix A(2, 3), B(2, 3), C;

    A(0, 0) = 1, A(0, 1) = 4, A(0, 2) = 1;
    A(1, 0) = 9, A(1, 1) = 8, A(1, 2) = 0;

    B(0, 0) = 8, B(0, 1) = 6, B(0, 2) = 1;
    B(1, 0) = 9, B(1, 1) = 2, B(1, 2) = 7;

    C = A - B;
    REQUIRE(C(0, 0) == -7);
    REQUIRE(C(0, 1) == -2);
    REQUIRE(C(0, 2) == 0);
    REQUIRE(C(1, 0) == 0);
    REQUIRE(C(1, 1) == 6);
    REQUIRE(C(1, 2) == -7);
}

TEST_CASE("matrix/multiplication_operator", "test multiplication of matrices") {
    Matrix A(2, 3), B(3, 2), C;

    A(0, 0) = 1, A(0, 1) = 4, A(0, 2) = 1;
    A(1, 0) = 9, A(1, 1) = 8, A(1, 2) = 0;

    B(0, 0) = 8, B(0, 1) = 6;
    B(1, 0) = 1, B(1, 1) = 9;
    B(2, 0) = 2, B(2, 1) = 7;

    C = A * B;
    REQUIRE(C(0, 0) == 14);
    REQUIRE(C(0, 1) == 49);
    REQUIRE(C(1, 0) == 80);
    REQUIRE(C(1, 1) == 126);
}

TEST_CASE("matrix/getSubmatrix", "test get submatrix") {
    Matrix A(2, 3), B;

    A(0, 0) = 1, A(0, 1) = 4, A(0, 2) = 1;
    A(1, 0) = 9, A(1, 1) = 8, A(1, 2) = 0;

    B = A.getSubmatrix(1, 1, 1, 1);
    REQUIRE(B.getSize() == make_pair((idx_t) 1, (idx_t) 1));
    REQUIRE(B(0, 0) == 8);

    B = A.getSubmatrix(0, 2, 1, 2);
    REQUIRE(B.getSize() == make_pair((idx_t) 2, (idx_t) 2));
    REQUIRE(B(0, 0) == 4);
    REQUIRE(B(0, 1) == 1);
    REQUIRE(B(1, 0) == 8);
    REQUIRE(B(1, 1) == 0);
}

TEST_CASE("matrix/setSubmatrix", "test set submatrix") {
    Matrix A(2, 3), B;

    A(0, 0) = 1, A(0, 1) = 4, A(0, 2) = 1;
    A(1, 0) = 9, A(1, 1) = 8, A(1, 2) = 0;

    B = Matrix(1, 1);
    B(0, 0) = 5;
    A.setSubmatrix(1, 1, B);
    REQUIRE(A(1, 1) == 5);

    B = Matrix(2, 2);
    B(0, 0) = 1, B(0, 1) = 2;
    B(1, 0) = 3, B(1, 1) = 4;
    A.setSubmatrix(0, 1, B);
    REQUIRE(A(0, 1) == 1);
    REQUIRE(A(0, 2) == 2);
    REQUIRE(A(1, 1) == 3);
    REQUIRE(A(1, 2) == 4);

    A.setSubmatrix(0, 2, 1, 2, 0);
    REQUIRE(A(0, 1) == 0);
    REQUIRE(A(0, 2) == 0);
    REQUIRE(A(1, 1) == 0);
    REQUIRE(A(1, 2) == 0);
}

TEST_CASE("matrix/transpose", "test transpose function") {
    Matrix A(2, 3), B;

    A(0, 0) = 1, A(0, 1) = 2, A(0, 2) = 3;
    A(1, 0) = 4, A(1, 1) = 5, A(1, 2) = 6;

    B = A.transpose();
    REQUIRE(B(0, 0) == A(0, 0));
    REQUIRE(B(0, 1) == A(1, 0));
    REQUIRE(B(1, 0) == A(0, 1));
    REQUIRE(B(1, 1) == A(1, 1));
    REQUIRE(B(2, 0) == A(0, 2));
    REQUIRE(B(2, 1) == A(1, 2));
}

TEST_CASE("matrix/identity", "test identity function") {
    Matrix I = Matrix::identity(5);
    for (idx_t i = 0; i < 5; i++)
        for (idx_t j = 0; j < 5; j++)
            if (i == j)
                REQUIRE(I(i, j) == 1);
            else
                REQUIRE(I(i, j) == 0);
    REQUIRE(I.isIdentity() == true);
}

TEST_CASE("matrix/null", "test null function") {
    Matrix N = Matrix::null(5, 5);
    for (idx_t i = 0; i < 5; i++)
        for (idx_t j = 0; j < 5; j++)
            REQUIRE(N(i, j) == 0);
    REQUIRE(N.isNull() == true);
}

/* END SECTION MATRIX */

/* SECTION SPARSE MATRIX */

TEST_CASE("sparsematrix/accessors", "test getter and setter") {
    SparseMatrix A(10, 10);

    A.set_elem(1, 1, 1);
    A.set_elem(1, 0, 2);
    A.set_elem(1, 3, 4);
    A.set_elem(1, 2, 3);
    A.set_elem(1, 3, 0);
    A.set_elem(3, 2, 10);

    REQUIRE(A.get_elem(1, 1) == 1);
    REQUIRE(A.get_elem(1, 0) == 2);
    REQUIRE(A.get_elem(1, 2) == 3);
    REQUIRE(A.get_elem(3, 2) == 10);
    REQUIRE(A.get_elem(0, 0) == 0);
    REQUIRE(A.get_elem(3, 1) == 0);
    REQUIRE_THROWS(A.get_elem(11, 10) == 1);
}

/* END SECTION SPARSE MATRIX */

/* SECTION VECTOR */

TEST_CASE("vector/accessors", "test getter and setter") {
    Vector v(10);

    v(1) = 1;
    v(3) = 10;

    REQUIRE(v(1) == 1);
    REQUIRE(v(3) == 10);
    REQUIRE_THROWS(v(11) == 1);
}

TEST_CASE("vector/sum_operator", "test sum of vectors") {
    Vector u(3), v(3), w;

    u(0) = 8, u(1) = 6, u(2) = 1;
    v(0) = 1, v(1) = 4, v(2) = 1;

    w = u + v;
    REQUIRE(w(0) == 9);
    REQUIRE(w(1) == 10);
    REQUIRE(w(2) == 2);
}

TEST_CASE("vector/subtraction_operator", "test subtraction of vectors") {
    Vector u(3), v(3), w;

    u(0) = 8, u(1) = 6, u(2) = 1;
    v(0) = 1, v(1) = 4, v(2) = 1;

    w = u - v;
    REQUIRE(w(0) == 7);
    REQUIRE(w(1) == 2);
    REQUIRE(w(2) == 0);
}

/* END SECTION VECTOR */

/* SECTION LINALG */

TEST_CASE("linalg/LU_decomposition", "test LU decomposition") {
    idx_t n = 10;
    Matrix A = Matrix::random(n, n), L, U;

    REQUIRE(LU_decomposition(A, L, U) == true);
    REQUIRE(L.isL() == true);
    REQUIRE(U.isU() == true);
    REQUIRE((A - L * U).isNull() == true);
}

TEST_CASE("linalg/det_LU", "test determinant of LU") {
    Matrix A(3, 3), L, U;

    A(0, 0) = 3; A(0, 1) = 2; A(0, 2) = 2;
    A(1, 0) = 6; A(1, 1) = 3; A(1, 2) = 4;
    A(2, 0) = 4; A(2, 1) = 5; A(2, 2) = 7;

    REQUIRE(LU_decomposition(A, L, U) == true);
    REQUIRE(cmp_number_t(det_LU(L, U), -13) == 0);
}

TEST_CASE("linalg/inverse_L", "test inverse of L") {
    Matrix I = Matrix::identity(3);
    Matrix L = Matrix::null(3, 3), L_1;

    L(0, 0) = 1;
    L(1, 0) = 2, L(1, 1) = 3;
    L(2, 0) = 4, L(2, 1) = 5, L(2, 2) = 6;

    REQUIRE(inverse_L(L, L_1) == true);
    REQUIRE((I - L_1 * L).isNull() == true);
    REQUIRE((I - L * L_1).isNull() == true);
}

TEST_CASE("linalg/inverse_U", "test inverse of U") {
    Matrix I = Matrix::identity(3);
    Matrix U = Matrix::null(3, 3), U_1;

    U(0, 0) = 1, U(0, 1) = 2, U(0, 2) = 3;
                 U(1, 1) = 4, U(1, 2) = 5;
                              U(2, 2) = 6;

    REQUIRE(inverse_U(U, U_1) == true);
    REQUIRE((I - U_1 * U).isNull() == true);
    REQUIRE((I - U * U_1).isNull() == true);
}

TEST_CASE("linalg/inverse", "test inverse") {
    Matrix I = Matrix::identity(3);
    Matrix A = Matrix::random(3, 3), A_1;

    REQUIRE(inverse(A, A_1) == true);
    REQUIRE((I - A * A_1).isNull());
    REQUIRE((I - A_1 * A).isNull());
}

TEST_CASE("linalg/solve_LU", "test linear system solver via LU") {
    idx_t n = 10;
    Matrix A = Matrix::random(n, n), L, U;
    Vector b = Vector::random(n), x, Ux;

    REQUIRE(LU_decomposition(A, L, U) == true);

    REQUIRE(solve_L(L, b, Ux) == true);
    REQUIRE((L * Ux - b).isNull() == true);

    REQUIRE(solve_U(U, Ux, x) == true);
    REQUIRE((U * x - Ux).isNull() == true);

    REQUIRE(solve_LU(L, U, b, x) == true);
    REQUIRE((A * x - b).isNull() == true);
}

TEST_CASE("linalg/cholesky_decomposition", "test cholesky decomposition") {
    idx_t n = 10;
    Matrix A = Matrix::randomPositive(n), L, Lt;

    REQUIRE(cholesky_decomposition(A, L, Lt) == true);
    REQUIRE(L.isL() == true);
    REQUIRE(Lt.isU() == true);
    REQUIRE((A - L * Lt).isNull() == true);

    REQUIRE(cholesky_decomposition_2(A, L, Lt) == true);
    REQUIRE(L.isL() == true);
    REQUIRE(Lt.isU() == true);
    REQUIRE((A - L * Lt).isNull() == true);
}

TEST_CASE("linalg/cholesky_block_decomposition", "test cholesky block decomposition") {
    idx_t n = 10;
    Matrix A = Matrix::randomPositive(n), L, Lt;

    REQUIRE(cholesky_block_decomposition(A, L, 3) == true);
    Lt = L.transpose();
    REQUIRE(L.isL() == true);
    REQUIRE(Lt.isU() == true);
    REQUIRE((A - L * Lt).isNull() == true);

    REQUIRE(cholesky_block_decomposition_2(A, L, 3) == true);
    Lt = L.transpose();
    REQUIRE(L.isL() == true);
    REQUIRE(Lt.isU() == true);
    REQUIRE((A - L * Lt).isNull() == true);
}

TEST_CASE("linalg/qr_gs_decomposition", "test qr gs decomposition") {
    idx_t n = 10;
    Matrix A = Matrix::random(n, n), Q, R;

    REQUIRE(QR_GS_decomposition(A, Q, R) == true);
    REQUIRE(Q.isQ() == true);
    REQUIRE(R.isU() == true);
    REQUIRE((A - Q * R).isNull() == true);
}

TEST_CASE("linalg/qr_hh_decomposition", "test qr hh decomposition") {
    SECTION("", "square matrices") {
        idx_t n = 10;
        Matrix A = Matrix::random(n, n), Q, R;

        REQUIRE(QR_HH_decomposition(A, Q, R) == true);
        REQUIRE(Q.isQ() == true);
        REQUIRE(R.isU() == true);
        REQUIRE((A - Q * R).isNull() == true);
    }
    SECTION("", "m < n matrices") {
        idx_t m = 5, n = 10;
        Matrix A = Matrix::random(m, n), Q, R;

        REQUIRE(QR_HH_decomposition(A, Q, R) == true);
        REQUIRE(Q.isQ() == true);
        REQUIRE(R.isU() == true);
        REQUIRE((A - Q * R).isNull() == true);
    }
    SECTION("", "m > n matrices") {
        idx_t m = 10, n = 5;
        Matrix A = Matrix::random(m, n), Q, R;

        REQUIRE(QR_HH_decomposition(A, Q, R) == true);
        REQUIRE(Q.isQ() == true);
        REQUIRE(R.isU() == true);
        REQUIRE((A - Q * R).isNull() == true);
    }
}

TEST_CASE("linalg/solve_QR", "test linear system solver via QR") {
    idx_t n = 10;
    Matrix A = Matrix::random(n, n), Q, R;
    Vector b = Vector::random(n), x;

    REQUIRE(QR_GS_decomposition(A, Q, R) == true);
    REQUIRE(solve_QR(Q, R, b, x) == true);
    REQUIRE((A * x - b).isNull() == true);

    REQUIRE(QR_HH_decomposition(A, Q, R) == true);
    REQUIRE(solve_QR(Q, R, b, x) == true);
    REQUIRE((A * x - b).isNull() == true);
}

TEST_CASE("linear/diag_QR", "test diagonalization using QR method") {
    idx_t n = 10;
    Matrix A = Matrix::randomPositive(n), D, P;

    REQUIRE(diag_QR(A, D, P, 1000, 1e-6) == true);
    REQUIRE((A * P - P * D).isNull() == true);
}

TEST_CASE("linalg/hessenberg_decomposition", "test hessenberg decomposition") {
    idx_t n = 10;
    Matrix A = Matrix::random(n, n), Q, H;

    REQUIRE(hessenberg_decomposition(A, Q, H) == true);
    REQUIRE(Q.isQ() == true);
    REQUIRE(H.isHessenberg() == true);
    REQUIRE((A - Q * H * Q.transpose()).isNull() == true);
}

TEST_CASE("linalg/lanczos", "test lanczos") {
    idx_t n = 8;
    SparseMatrix A = SparseMatrix::randomSymmetric(n);
    Vector lambda;
    Matrix eigv;

    puts("Lanczos");
    A.print(false, 10);
    puts("");

    if (!lanczos_eigenpairs(A, lambda, eigv, 4))
        puts("fail");

    lambda.print(false, 10);
    puts("");
    eigv.print(false, 10);
    puts("");

    Matrix AA(n, n), D, P;
    for (idx_t i = 0; i < n; i++) {
        AA(i, i) = A.get_elem(i, i);
        for (idx_t j = 0; j < n; j++)
            AA(i, j) = AA(j, i) = A.get_elem(i, j);
    }
    diag_QR(AA, D, P, 1000, 1e-4);
    D.print(false, 10);
    puts("");
    P.print(false, 10);
    puts("");
}

TEST_CASE("linalg/arnoldi", "test arnoldi") {
    idx_t n = 6;
    SparseMatrix A = SparseMatrix::randomSymmetric(n, 3, -1, 1);
    Vector lambda;
    Matrix eigv;

    puts("Arnoldi");
    A.print(false, 10);
    puts("");

    if (!arnoldi_eigenpairs(A, lambda, eigv, 3))
        puts("fail");

    lambda.print(false, 10);
    puts("");
    eigv.print(false, 10);
    puts("");

    Matrix AA(n, n), D, P;
    for (idx_t i = 0; i < n; i++) {
        AA(i, i) = A.get_elem(i, i);
        for (idx_t j = 0; j < n; j++)
            AA(i, j) = AA(j, i) = A.get_elem(i, j);
    }
    diag_QR(AA, D, P, 1000, 1e-4);
    D.print(false, 10);
    puts("");
    P.print(false, 10);
    puts("");
}

/* END SECTION LINALG */
