#include "vector.h"
#include "matrix.h"
#include "sparsematrix.h"
#include "linalg.h"

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>

using namespace std;

int main(int argc, char *argv[])
{
    clock_t s = clock();

    idx_t n = 6;
    SparseMatrix A = SparseMatrix::randomSymmetric(n, 3, -1, 1);
    Vector lambda;
    Matrix eigv;

    puts("Arnoldi");
    A.print(false, 10);
    puts("");

    if (!arnoldi_eigenpairs(A, lambda, eigv, 4))
        puts("fail");

    lambda.print(false, 10);
    puts("");

    eigv.print(false, 10);
    puts("");

    Matrix AA(n, n), D, P;
    for (idx_t i = 0; i < n; i++) {
        AA(i, i) = A.get_elem(i, i);
        for (idx_t j = 0; j < n; j++)
            AA(i, j) = AA(j, i) = A.get_elem(i, j);
    }

    diag_QR(AA, D, P, 1000, 1e-4);
    D.print(false, 10);
    puts("");
    P.print(false, 10);
    puts("");
    
    printf("Total: %lf s\n", (double) (clock() - s) / CLOCKS_PER_SEC);

    return 0;
}
